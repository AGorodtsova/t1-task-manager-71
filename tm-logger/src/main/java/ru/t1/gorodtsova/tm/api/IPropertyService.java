package ru.t1.gorodtsova.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getBrokerUrl();

    @NotNull
    String getMongoHost();

    @NotNull
    Integer getMongoPort();

}
