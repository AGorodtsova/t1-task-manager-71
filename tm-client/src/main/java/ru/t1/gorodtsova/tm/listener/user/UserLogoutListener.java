package ru.t1.gorodtsova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @NotNull
    private final String DESCRIPTION = "Logout current user";

    @NotNull
    private final String NAME = "logout";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userLogoutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        authEndpoint.logout(request);
        setToken(null);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
