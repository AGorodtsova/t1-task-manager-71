package ru.t1.gorodtsova.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.api.endpoint.*;
import ru.t1.gorodtsova.tm.api.service.ILoggerService;
import ru.t1.gorodtsova.tm.api.service.IPropertyService;
import ru.t1.gorodtsova.tm.api.service.IServiceLocator;
import ru.t1.gorodtsova.tm.api.service.ITokenService;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;
import ru.t1.gorodtsova.tm.listener.AbstractListener;
import ru.t1.gorodtsova.tm.util.SystemUtil;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Getter
    @Autowired
    private AbstractListener[] listeners;

    @Getter
    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK_MANAGER IS SHUTTING DOWN **");
    }

    public void start(@Nullable String[] args) {
        processArguments(args);
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final RuntimeException e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processArguments(@Nullable final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.exit(0);
        } catch (@NotNull final RuntimeException e) {
            loggerService.error(e);
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    private void processArgument(@NotNull final String argument) {
        for (AbstractListener listener : listeners) {
            if (argument.equals(listener.getArgument())) {
                publisher.publishEvent(new ConsoleEvent(listener.getName()));
            }
        }
    }

}
