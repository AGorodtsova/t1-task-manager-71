package ru.t1.gorodtsova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.api.IListener;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    private final String ARGUMENT = "-arg";

    @NotNull
    private final String DESCRIPTION = "Show argument list";

    @NotNull
    private final String NAME = "arguments";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@argumentListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        for (@Nullable final IListener listener : listeners) {
            if (listener == null) continue;
            @Nullable final String argument = listener.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
