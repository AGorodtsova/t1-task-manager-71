package ru.t1.gorodtsova.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.gorodtsova.tm.config.WebApplicationConfiguration;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.model.Project;

import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class})
public class ProjectEndpointTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    private final static String BASE_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private final Project project1 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project2 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project3 = new Project(UUID.randomUUID().toString());

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        save(project1);
        save(project2);
    }

    @After
    @SneakyThrows
    public void clean() {
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_URL + "deleteAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void save(@NotNull final Project project) {
        @NotNull String url = BASE_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private Project findById(@NotNull final String id) {
        @NotNull String url = BASE_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Project.class);
    }

    @SneakyThrows
    private List<Project> findAll() {
        @NotNull String url = BASE_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, Project[].class));
    }

    @Test
    public void saveTest() {
        save(project3);
        @Nullable final Project project = findById(project3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project3.getId(), project.getId());
    }

    @Test
    public void findAllTest() {
        @Nullable final List<Project> projects = findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final Project project = findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull String url = BASE_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project2);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project2.getId()));
    }

    @Test
    @SneakyThrows
    public void deleteByIdTest() {
        @NotNull String url = BASE_URL + "deleteById/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project1.getId()));
    }

}
