package ru.t1.gorodtsova.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.gorodtsova.tm.dto.model.AbstractModelDTO;

public interface IDtoRepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

}
