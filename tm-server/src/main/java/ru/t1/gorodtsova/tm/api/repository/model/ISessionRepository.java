package ru.t1.gorodtsova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.gorodtsova.tm.model.Session;

import java.util.List;

@Repository
public interface ISessionRepository extends IUserOwnedRepository<Session> {

    @NotNull
    List<Session> findByUserId(@NotNull String userId);

    @Nullable
    Session findOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserId(@NotNull String userId);

    void deleteOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

}
